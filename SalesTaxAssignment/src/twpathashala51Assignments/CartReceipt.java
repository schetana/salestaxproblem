package twpathashala51Assignments;

import java.util.ArrayList;

import static twpathashala51Assignments.Good.TypeOfGood.OTHERS;

class CartReceipt {
    private final ArrayList<Good> itemsInCart;
    Money salesTax;
    Money totalBill;

    private CartReceipt(ArrayList<Good> goods, Money salesTax, Money totalBill) {
        itemsInCart = goods;
        this.salesTax = salesTax;
        this.totalBill = totalBill;
    }

    static CartReceipt initialCartReceipt(ArrayList<Good> goods) {
        return new CartReceipt(goods, Money.createMoney(0), Money.createMoney(0));
    }

    static CartReceipt initialCartReceipt(ArrayList<Good> goods, Money salesTax, Money totalBill) {
        return new CartReceipt(goods, salesTax, totalBill);
    }

    ArrayList<Money> getFinalPriceList() {
        ArrayList<Money> finalPriceList = new ArrayList<>();
        int n;
        Money finalPrice;
        for (Good item : itemsInCart) {
            n = 0;
            if (item.getType().equals(OTHERS))
                n = 10;
            if (item.isImported())
                n += 5;
            salesTax =salesTax.addMoney(item.nPercentOfPrice(n));
            finalPrice = item.pricePlusNPercentOfPrice(n).formatValue();
            totalBill=totalBill.addMoney(finalPrice);
            finalPriceList.add(finalPrice);
        }
        return finalPriceList;
    }

    Money calculateSalesTax() {
        return salesTax.formatValue();
    }

    Money getTotalBill() {
         return totalBill.formatValue();
    }

    CartReceipt getFinalReceipt() {
        ArrayList<Money> finalPriceList = getFinalPriceList();
        ArrayList<Good> goodList = new ArrayList<>();
        int i = 0;
        for (Good item : itemsInCart) {
            goodList.add(Good.createGood(item.getQuantity(), item.name, item.isImported(), item.getType(), finalPriceList.get
                    (i)));
            i++;
        }
        CartReceipt outputReceipt = initialCartReceipt(goodList);
        outputReceipt.salesTax = this.calculateSalesTax();
        outputReceipt.totalBill = this.getTotalBill();
        return outputReceipt;
    }


    @Override
    public String toString() {
        return itemsInCart+"\nSales Tax:"+salesTax+"\nTotal Bill:"+totalBill+"\n";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CartReceipt that = (CartReceipt) o;

        if (itemsInCart != null ? !itemsInCart.equals(that.itemsInCart) : that.itemsInCart != null) return false;
        if (salesTax != null ? !salesTax.equals(that.salesTax) : that.salesTax != null) return false;
        return totalBill != null ? totalBill.equals(that.totalBill) : that.totalBill == null;

    }

    @Override
    public int hashCode() {
        int result = itemsInCart != null ? itemsInCart.hashCode() : 0;
        result = 31 * result + (salesTax != null ? salesTax.hashCode() : 0);
        result = 31 * result + (totalBill != null ? totalBill.hashCode() : 0);
        return result;
    }
}


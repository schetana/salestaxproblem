package twpathashala51Assignments;

import java.io.*;
import java.util.ArrayList;

import static twpathashala51Assignments.CartReceipt.initialCartReceipt;
import static twpathashala51Assignments.Good.TypeOfGood.FOOD;
import static twpathashala51Assignments.Good.TypeOfGood.MEDICAL_PRODUCT;
import static twpathashala51Assignments.Good.TypeOfGood.OTHERS;
import static twpathashala51Assignments.Good.createGood;

public class SalesTaxProblem {

    public static void main(String[] args) throws IOException {
            try {
                Good perfume = createGood("Engage", true, OTHERS, Money.createMoney(27.99));
                Good perfume2 = createGood("Faa", false, OTHERS, Money.createMoney(18.99));
                Good tablets = createGood("Headache Pills", false, MEDICAL_PRODUCT, Money.createMoney(9.75));
                Good chocolate = createGood("Five Star", true, FOOD, Money.createMoney(11.25));
                ArrayList<Good> itemsInCart = new ArrayList<>();
                itemsInCart.add(perfume);
                itemsInCart.add(perfume2);
                itemsInCart.add(tablets);
                itemsInCart.add(chocolate);
                CartReceipt inputCart = initialCartReceipt(itemsInCart);

                Good importedPerfume = createGood("Engagae Cologne", true, OTHERS, Money.createMoney(47.50));
                Good importedChocolate = createGood("Five Star", true, FOOD, Money.createMoney(10.00));
                ArrayList<Good> importedItems = new ArrayList<>();
                importedItems.add(importedPerfume);
                importedItems.add(importedChocolate);
                CartReceipt importedCart = initialCartReceipt(importedItems);

                CartReceipt outputCartReceipt1 = importedCart.getFinalReceipt();
                CartReceipt outputCartReceipt2 = inputCart.getFinalReceipt();
                File outfile = new File("/Users/schetana/IdeaProjects/salestaxproblem/SalesTaxAssignment/src/twpathashala51Assignments/Output_File");
                FileWriter fileWriter = new FileWriter(outfile);
                BufferedWriter outputWriter = new BufferedWriter(fileWriter);
                outputWriter.write(outputCartReceipt1.toString());
                outputWriter.write("\n\n");
                outputWriter.write(outputCartReceipt2.toString());
                outputWriter.close();

            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }



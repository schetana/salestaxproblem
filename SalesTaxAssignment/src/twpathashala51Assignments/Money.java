package twpathashala51Assignments;

import java.text.DecimalFormat;

public class Money {

    double value;

    Money(double value) {
        this.value = value;
    }

    static Money createMoney(double value) {
        return new Money(value);
    }

    Money pricePlusNPercentOfMoney(int n) {
        double totalPriceIncludingPercent;
        totalPriceIncludingPercent = (this.addMoney(nPercentOfMoney(n)).value);
        return createMoney(totalPriceIncludingPercent);
    }

    Money nPercentOfMoney(int percent) {
        double fraction = (value * percent) / 100;
        return Money.createMoney(roundUptoPointZeroFive(fraction));
    }

    double roundUptoPointZeroFive(double value) {
        return Math.ceil(value * 20) / 20;
    }

    Money addMoney(Money other) {
        return createMoney(this.value + other.value);
    }

     Money multiply(int quantity) {
        return createMoney(this.value * quantity);
    }

    Money formatValue() {
        return new Money(Double.parseDouble(new DecimalFormat("##.##").format(this.value)));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Money money = (Money) o;

        return Double.compare(money.value, value) == 0;

    }

    @Override
    public int hashCode() {
        long temp = Double.doubleToLongBits(value);
        return (int) (temp ^ (temp >>> 32));
    }

    @Override
    public String toString() {
        return String.format("%-10f",value);
    }
}

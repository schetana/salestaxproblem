package twpathashala51Assignments;

class Good {
    private static final int DEFAULT_QUANTITY = 1;
    final String name;
    private final Money price;
    private final boolean isImported;
    private final TypeOfGood type;
    private final int quantity;

    enum TypeOfGood {
        BOOK, FOOD, MEDICAL_PRODUCT, OTHERS;
    }

    private Good(int quantity, String name, boolean isImported, TypeOfGood type, Money price) {
        this.name = name;
        this.price = price;
        this.isImported = isImported;
        this.type = type;
        this.quantity = quantity;
    }
    static Good createGood(String name, boolean isImported, TypeOfGood type, Money price) {
        return new Good(DEFAULT_QUANTITY, name, isImported, type, price);
    }

    static Good createGood(int quantity, String name, boolean isImported, TypeOfGood type, Money price) {
        return new Good(quantity, name, isImported, type, price);
    }

    boolean isImported() {
        return isImported;
    }

    Money nPercentOfPrice(int n) {
        Money totalPrice = this.price.multiply(quantity);
        return totalPrice.nPercentOfMoney(n);
    }
    Money pricePlusNPercentOfPrice(int n) {
        Money totalPrice = this.price.multiply(quantity);
        return totalPrice.pricePlusNPercentOfMoney(n);
    }

    TypeOfGood getType() {
        return type;
    }

     int getQuantity() {
        return quantity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Good good = (Good) o;

        if (isImported != good.isImported) return false;
        if (quantity != good.quantity) return false;
        if (name != null ? !name.equals(good.name) : good.name != null) return false;
        if (price != null ? !price.equals(good.price) : good.price != null) return false;
        return type == good.type;

    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (price != null ? price.hashCode() : 0);
        result = 31 * result + (isImported ? 1 : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + quantity;
        return result;
    }

    @Override
    public String toString() {
        return "\n"+String.format("%-20s %-20s %-40s %-10d",name,isImported,type,quantity)+price;
    }
}



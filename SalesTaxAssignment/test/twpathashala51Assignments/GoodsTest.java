package twpathashala51Assignments;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static twpathashala51Assignments.Good.TypeOfGood;
import static twpathashala51Assignments.Good.TypeOfGood.BOOK;
import static twpathashala51Assignments.Good.TypeOfGood.OTHERS;
import static twpathashala51Assignments.Good.createGood;

public class GoodsTest {
    @Test
    public void shouldGetTrueForImportedGoods() {
        String name = "book";
        Money price = Money.createMoney(12.49);
        boolean isImported = true;
        TypeOfGood type = BOOK;
        assertTrue(createGood(name, isImported, type, price).isImported());
    }

    @Test
    public void shouldGetBOOKAsTypeOfGoodForABook() {
        String name = "book";
        Money price = Money.createMoney(12.49);
        boolean isImported = true;
        TypeOfGood type = BOOK;
        assertEquals(BOOK, createGood(name, isImported, type, price).getType());
    }

    @Test
    public void shouldGetSpecifiedPercentageOfPrice() {
        String name = "perfume";
        Money price = Money.createMoney(47.5);
        boolean isImported = true;
        TypeOfGood type = OTHERS;
        int percentage = 15;
        Money expectedOutput = Money.createMoney(7.15);
        assertEquals(expectedOutput, createGood(name, isImported, type, price).nPercentOfPrice(percentage));
    }

    @Test
    public void shouldGetTotalPriceIncludingSomePercentOfPrice() {
        String name = "CD";
        Money price = Money.createMoney(14.99);
        boolean isImported = true;
        TypeOfGood type = OTHERS;
        int percentage = 10;
        assertEquals(Money.createMoney(16.49), createGood(name, isImported, type, price).pricePlusNPercentOfPrice
                (percentage).formatValue());
    }


}

package twpathashala51Assignments;

import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static twpathashala51Assignments.CartReceipt.initialCartReceipt;
import static twpathashala51Assignments.Good.TypeOfGood.*;
import static twpathashala51Assignments.Good.createGood;

public class CartReceiptTest {

    private CartReceipt createACartWithBookCDChocolate() {
        Good book = createGood("book", false, BOOK, Money.createMoney(12.49));
        Good cd = createGood("CD", false, OTHERS, Money.createMoney(14.99));
        Good chocolate = createGood("Five Star", false, FOOD, Money.createMoney(0.85));
        ArrayList<Good> itemsInCart = new ArrayList<>();
        itemsInCart.add(book);
        itemsInCart.add(cd);
        itemsInCart.add(chocolate);

        return initialCartReceipt(itemsInCart);
    }

    @Test
    public void shouldCreateACartWithGoods() {
        assertNotNull(createACartWithBookCDChocolate());
    }

    @Test
    public void shouldGetSalesTaxForAllItemsInCart() {
        CartReceipt cartReceipt= createACartWithBookCDChocolate();
        cartReceipt.getFinalPriceList();
        assertEquals(Money.createMoney(1.50), cartReceipt.calculateSalesTax());
    }

    @Test
    public void shouldGetFinalPriceListOfItems() {
        ArrayList<Money> finalPriceList = new ArrayList<>();
        finalPriceList.add(Money.createMoney(12.49));
        finalPriceList.add(Money.createMoney(16.49));
        finalPriceList.add(Money.createMoney(0.85));

        assertEquals(finalPriceList, createACartWithBookCDChocolate().getFinalPriceList());
    }

    @Test
    public void shouldGetFinalPriceListOfImportedItems() {
        Good chocolates = createGood("asd", true, FOOD, Money.createMoney(10));
        Good perfume = createGood("qwe", true, OTHERS, Money.createMoney(47.50));
        ArrayList<Good> itemsInCart = new ArrayList<>();
        itemsInCart.add(chocolates);
        itemsInCart.add(perfume);
        ArrayList<Money> finalPriceList = new ArrayList<>();
        finalPriceList.add(Money.createMoney(10.50));
        finalPriceList.add(Money.createMoney(54.65));

        assertEquals(finalPriceList, initialCartReceipt(itemsInCart).getFinalPriceList());
    }

    @Test
    public void shouldGetTotalBillOfListOfItems() {
        CartReceipt cart = createACartWithBookCDChocolate();
        cart.getFinalPriceList();
        assertEquals(Money.createMoney(29.83), cart.getTotalBill());
    }

    @Test
    public void shouldGetListOfItemsWithFinalPrice() {
        Good perfume = createGood("Engage", true, OTHERS, Money.createMoney(27.99));
        Good perfume2 = createGood("Faa", false, OTHERS, Money.createMoney(18.99));
        Good tablets = createGood("Headache Pills", false, MEDICAL_PRODUCT, Money.createMoney(9.75));
        Good chocolate = createGood("Five Star", true, FOOD, Money.createMoney(11.25));

        ArrayList<Good> itemsInCart = new ArrayList<>();
        itemsInCart.add(perfume);
        itemsInCart.add(perfume2);
        itemsInCart.add(tablets);
        itemsInCart.add(chocolate);
        CartReceipt cart = initialCartReceipt(itemsInCart);

        ArrayList<Good> finalReceiptList = new ArrayList<>();
        Good perfumeInReceipt = createGood(1, "Engage", true, OTHERS, Money.createMoney(32.19));
        Good perfume2InReceipt = createGood(1, "Faa", false, OTHERS, Money.createMoney(20.89));
        Good tabletsInReceipt = createGood(1, "Headache Pills", false, MEDICAL_PRODUCT, Money.createMoney(9.75));
        Good chocolateInReceipt = createGood(1, "Five Star", true, FOOD, Money.createMoney(11.85));
        finalReceiptList.add(perfumeInReceipt);
        finalReceiptList.add(perfume2InReceipt);
        finalReceiptList.add(tabletsInReceipt);
        finalReceiptList.add(chocolateInReceipt);
        CartReceipt expectedCartReceipt = initialCartReceipt(finalReceiptList, Money.createMoney(6.70), Money.createMoney(74.68));
        assertEquals(expectedCartReceipt, cart.getFinalReceipt());
    }
}
